<h2>Merge Request guidelines</h2>

- [ ] Adhering to coding style
- [ ] Clear naming
- [ ] Code duplication
- [ ] Documentation
- [ ] Maintainability
- [ ] Pipeline ran successfully
    - [ ] Tests successful    
