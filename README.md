[![pipeline status](https://gitlab.com/gudmundur_bjornsson/crmtemplate/badges/main/pipeline.svg)](https://gitlab.com/gudmundur_bjornsson/crmtemplate/-/commits/main)
[![coverage report](https://gitlab.com/gudmundur_bjornsson/crmtemplate/badges/main/coverage.svg)](https://gitlab.com/gudmundur_bjornsson/crmtemplate/-/commits/main)
[![Quality Gate Status](https://sonarcloud.io/api/project_badges/measure?project=gudmundur_bjornsson_crmtemplate&metric=alert_status)](https://sonarcloud.io/summary/new_code?id=gudmundur_bjornsson_crmtemplate)

# Flow CRM Template

The idea is to create a simple CRM module, using Vaadin tutorial project as a starting point.  
This template will be handling list of students.  

Main purpose of this codebase is to be used for Software Quality course taught at University of Iceland

This project is not to be used for production.

## Running the application

The project is a standard Maven project. To run it from the command line,
type `mvnw` (Windows), or `./mvnw` (Mac & Linux), then open
http://localhost:8080 in your browser.

## Deploying to Production

To create a production build, call `mvnw clean package -Pproduction` (Windows),
or `./mvnw clean package -Pproduction` (Mac & Linux).
This will build a JAR file with all the dependencies and front-end resources,
ready to be deployed. The file can be found in the `target` folder after the build completes.

Once the JAR file is built, you can run it using
`java -jar target/flowcrmtutorial-1.0-SNAPSHOT.jar`

## Software Quality Management Plan

[See our Wiki page](https://gitlab.com/gudmundur_bjornsson/crmtemplate/-/wikis/software-quality-management-plan)

## LICENSE

[The Unlicense](https://gitlab.com/gudmundur_bjornsson/crmtemplate/-/blob/main/LICENSE.md) 

## Build 

If you want to use CRM-template, you need to [setup maven](https://www.tutorialspoint.com/maven/maven_environment_setup.htm)
Then run `mvn compile` to compile the project or `mvn test` to run tests and compile the project.

If the intention is to build for jar file, you need to execute `mvn package` which makes a jar file stored in target directory.


