package com.template.application.views.list;
/*
import com.template.application.data.entity.Contact;
import com.vaadin.flow.component.grid.Grid;
import com.vaadin.flow.data.provider.ListDataProvider;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

@RunWith(SpringRunner.class)
@SpringBootTest
public class ListViewTest {

    @Autowired
    private ListView listView;

    @Test
    public void formShownWhenContactSelected(){
        Grid<Contact> grid = listView.grid;
        Contact firstContact = getFirstItem(grid);

        ContactForm form = listView.form;
        //Assert that our form is not visible in the start
        Assert.assertFalse(form.isVisible());
        grid.asSingleSelect().setValue(firstContact);
        //Assert after programatically selecting one contact, that form is visible
        Assert.assertTrue(form.isVisible());
        //Assert that our selected first contact is same as database first contact
        Assert.assertEquals(firstContact.getFirstName(), form.firstName.getValue());
    }
    private Contact getFirstItem(Grid<Contact> grid) {
        return ((ListDataProvider<Contact>)grid.getDataProvider()).getItems().iterator().next();
    }
}
*/